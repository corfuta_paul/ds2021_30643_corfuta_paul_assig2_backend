package ro.tuc.ds2020;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import ro.tuc.ds2020.services.MonitoredValueService;
import ro.tuc.ds2020.dtos.MonitoredValueDTO;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Component
public class Consumer {

    private final static String QUEUE_NAME = "MeasurementQueue";

    private final MonitoredValueService monitoredValueService;

    @Autowired
    private Consumer(MonitoredValueService monitoredValueService){
        this.monitoredValueService=monitoredValueService;
    }

    @Scheduled(fixedRate = 1000)
    public void Receive() throws Exception {

        String url = System.getenv("CLOUDAMQP_URL");
        if (url == null){
            url = "amqps://qreumpxs:KHNV_hxlizr0YgYyx78YwK-FXqtPrqA8@cow.rmq2.cloudamqp.com/qreumpxs";
        }
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(url);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println("Waiting for messages.");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            System.out.println("Received :" + message + " .");
            String[] arrOfStr = message.split("[,{}\"]+");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime dateTime = LocalDateTime.parse(arrOfStr[3].substring(0,19), formatter);
            Date date = java.sql.Timestamp.valueOf(dateTime);
            MonitoredValueDTO monitoredValueDTO = new MonitoredValueDTO(0, date,(long)Double.parseDouble(arrOfStr[9]), Long.parseLong(arrOfStr[6]));
            monitoredValueService.insertMonitoredValue(monitoredValueDTO);
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> {
        });


    }
}