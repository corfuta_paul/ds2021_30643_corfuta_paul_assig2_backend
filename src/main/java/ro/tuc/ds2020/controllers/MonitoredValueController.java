package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MonitoredValueDTO;
import ro.tuc.ds2020.entities.MonitoredValue;
import ro.tuc.ds2020.services.MonitoredValueService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/monitoredvalue")
public class MonitoredValueController {
    private final MonitoredValueService monitoredValueService;

    @Autowired
    private MonitoredValueController(MonitoredValueService monitoredValueService){
        this.monitoredValueService=monitoredValueService;
    }

    @GetMapping
    public ResponseEntity<List<MonitoredValueDTO>> findMonitoredValues(){
        List<MonitoredValueDTO> dtos = monitoredValueService.findMonitoredValues();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MonitoredValueDTO> findMonitoredValueById(@PathVariable long id){
        MonitoredValueDTO monitoredValueDTO = monitoredValueService.findMonitoredValueById(id);

        return new ResponseEntity<>(monitoredValueDTO,HttpStatus.OK);
    }

    @GetMapping("/sensor/{id}")
    public ResponseEntity<List<MonitoredValueDTO>> findMonitoredValueBySensorId(@PathVariable long id){
        List<MonitoredValueDTO> monitoredValuesDTO = monitoredValueService.findMonitoredValuesBySensorId(id);

        return new ResponseEntity<>(monitoredValuesDTO,HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Long> insertMonitoredValue(@Valid @RequestBody MonitoredValueDTO monitoredValueDTO){
        long id = monitoredValueService.insertMonitoredValue(monitoredValueDTO);

        return new ResponseEntity<>(id,HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteMonitoredValue(@PathVariable long id){
        monitoredValueService.deleteMonitoredValue(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<Void> updateMonitoredValue(@Valid @RequestBody MonitoredValueDTO monitoredValueDTO){
        monitoredValueService.updateMonitoredValue(monitoredValueDTO);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
