package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.ClientDTO;
import ro.tuc.ds2020.services.ClientService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/client")
public class ClientController {
    private final ClientService clientService;

    @Autowired
    private ClientController(ClientService clientService){
        this.clientService=clientService;
    }

    @GetMapping()
    public ResponseEntity<List<ClientDTO>> getClients() {
        List<ClientDTO> dtos = clientService.findClients();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ClientDTO> getClientById(@PathVariable long id) {
        ClientDTO clientDTO = clientService.findClientById(id);

        return  new ResponseEntity<>(clientDTO, HttpStatus.OK);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<ClientDTO> getClientByUserId(@PathVariable long id) {
        ClientDTO clientDTO = clientService.findClientByUserId(id);

        return  new ResponseEntity<>(clientDTO, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Long> insertClient(@Valid @RequestBody ClientDTO clientDTO) {
        Long clientId = clientService.insertClient(clientDTO);
        return new ResponseEntity<>(clientId, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteClient(@PathVariable long id){
        clientService.deleteClient(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<Void> updateClient(@Valid @RequestBody ClientDTO clientDTO){
        clientService.updateClient(clientDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
