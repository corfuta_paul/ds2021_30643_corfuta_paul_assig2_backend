package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.services.SensorService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/sensor")
public class SensorController {
    private final SensorService sensorService;

    @Autowired
    private SensorController(SensorService sensorService){
        this.sensorService=sensorService;
    }

    @GetMapping
    public ResponseEntity<List<SensorDTO>> findSensors(){
        List<SensorDTO> dtos = sensorService.findSensors();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SensorDTO> findSensorById(@PathVariable long id){
        SensorDTO sensorDTO = sensorService.findSensorById(id);

        return new ResponseEntity<>(sensorDTO,HttpStatus.OK);
    }

    @GetMapping("/device/{id}")
    public ResponseEntity<SensorDTO> findSensorByDeviceId(@PathVariable long id){
        SensorDTO sensorDTO = sensorService.findSensorByDeviceId(id);

        return new ResponseEntity<>(sensorDTO,HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Long> insertSensor(@RequestBody SensorDTO sensorDTO){
        long id = sensorService.insertSensor(sensorDTO);

        return new ResponseEntity<>(id,HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSensor(@PathVariable long id){
        sensorService.deleteSensor(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<Void> updateSensor(@Valid @RequestBody SensorDTO sensorDTO){
        sensorService.updateSensor(sensorDTO);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
