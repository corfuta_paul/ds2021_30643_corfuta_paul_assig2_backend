package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.services.DeviceService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/device")
public class DeviceController {
    private final DeviceService deviceService;

    @Autowired
    private DeviceController(DeviceService deviceService){
        this.deviceService=deviceService;
    }

    @GetMapping
    public ResponseEntity<List<DeviceDTO>> findDevices(){
        List<DeviceDTO> dtos = deviceService.findDevices();

        return new ResponseEntity<>(dtos,HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<DeviceDTO> findDeviceById(@PathVariable long id){
        DeviceDTO deviceDTO = deviceService.findDeviceById(id);

        return new ResponseEntity<>(deviceDTO,HttpStatus.OK);
    }

    @GetMapping("/client/{id}")
    public ResponseEntity<List<DeviceDTO>> findDevicesByClientId(@PathVariable long id){
        List<DeviceDTO> dtos = deviceService.findDevicesByClientId(id);

        return new ResponseEntity<>(dtos,HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Long> insertDevice(@RequestBody DeviceDTO deviceDTO){
        long id = deviceService.insertDevice(deviceDTO);

        return new ResponseEntity<>(id,HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteDevice(@PathVariable long id){
        deviceService.deleteDevice(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<Void> updateDevice(@Valid @RequestBody DeviceDTO deviceDTO){
        deviceService.updateDevice(deviceDTO);

        return new ResponseEntity<>(HttpStatus.OK);
    }
    @GetMapping("maxenergy/{id}")
    public ResponseEntity<Long> findMaximumConsumption(@PathVariable long id){
        long max_energy = deviceService.findMaximumConsumptionbyDevice(id);

        return new ResponseEntity<>(max_energy, HttpStatus.OK);
    }

}
