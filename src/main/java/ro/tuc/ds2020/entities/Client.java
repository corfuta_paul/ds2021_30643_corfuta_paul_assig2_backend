package ro.tuc.ds2020.entities;

import javax.persistence.*;
import javax.persistence.Entity;

import java.util.Date;

import lombok.*;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "birth_date", nullable = false)
    private Date birthDate;

    @Column(name = "address", nullable = false)
    private String address;

    @OneToOne
    @JoinColumn(name="user_id",referencedColumnName = "id",unique = true)
    private ApplicationUser user;
}
