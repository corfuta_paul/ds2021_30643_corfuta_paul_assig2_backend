package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.MonitoredValue;
import ro.tuc.ds2020.entities.Sensor;

import java.util.List;

public interface MonitoredValueRepository extends JpaRepository<MonitoredValue,Long> {
    List<MonitoredValue> findMonitoredValuesBySensor(Sensor sensor);
}
