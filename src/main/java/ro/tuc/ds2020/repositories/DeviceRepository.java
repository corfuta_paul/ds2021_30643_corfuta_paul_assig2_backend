package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.Client;
import ro.tuc.ds2020.entities.Device;

import java.util.List;

public interface DeviceRepository extends JpaRepository<Device,Long> {
    @Query(value = "SELECT Max(m.energyConsumption) " +
            "FROM Client c " +
            "JOIN Device d on c.id=d.client.id " +
            "Join Sensor s on d.id=s.device.id " +
            "Join MonitoredValue m on s.id=m.sensor.id " +
            "Where d.id=:id "
    )
    Long findMaximumConsumptionByDevice(@Param("id") Long id);

    @Query(value = "SELECT AVG(m.energyConsumption) " +
            "FROM Client c " +
            "JOIN Device d on c.id=d.client.id " +
            "Join Sensor s on d.id=s.device.id " +
            "Join MonitoredValue m on s.id=m.sensor.id " +
            "Where d.id=:id "
    )
    Long findAverageConsumptionByDevice(@Param("id") Long id);

    List<Device> findDevicesByClient(Client client);
}
