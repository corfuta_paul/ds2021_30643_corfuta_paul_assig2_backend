package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.ApplicationUser;
import ro.tuc.ds2020.entities.Client;
import ro.tuc.ds2020.entities.Sensor;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long>{
    Client findClientByUser(ApplicationUser user);


}
