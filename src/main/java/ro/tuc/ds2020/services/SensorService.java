package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.MonitoredValueDTO;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.dtos.builders.ClientBuilder;
import ro.tuc.ds2020.dtos.builders.MonitoredValueBuilder;
import ro.tuc.ds2020.dtos.builders.SensorBuilder;
import ro.tuc.ds2020.entities.Client;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.MonitoredValue;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.MonitoredValueRepository;
import ro.tuc.ds2020.repositories.SensorRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SensorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SensorService.class);
    private final SensorRepository sensorRepository;
    private final DeviceRepository deviceRepository;
    private final MonitoredValueService monitoredValueService;

    @Autowired
    private SensorService(SensorRepository sensorRepository, DeviceRepository deviceRepository,MonitoredValueService monitoredValueService){
     this.sensorRepository=sensorRepository;
     this.deviceRepository=deviceRepository;
     this.monitoredValueService=monitoredValueService;
    }

    public List<SensorDTO> findSensors(){
        List<Sensor> sensorList = sensorRepository.findAll();
        return sensorList.stream()
                .map(SensorBuilder::ToSensorDTO)
                .collect(Collectors.toList());
    }

    public SensorDTO findSensorById(long id){
        Optional<Sensor> sensor = sensorRepository.findById(id);
        if(!sensor.isPresent()){
            LOGGER.error("Sensor with id {} was not found in db", id);
            throw new ResourceNotFoundException(Sensor.class.getSimpleName() + " with id: " + id);
        }
        return SensorBuilder.ToSensorDTO(sensor.get());
    }

    public SensorDTO findSensorByDeviceId(long id){
        Device device = deviceRepository.findById(id).get();
        Sensor sensor = sensorRepository.findSensorByDevice(device);
        if(sensor!=null)
           return SensorBuilder.ToSensorDTO(sensor);
        return null;
    }

    public long insertSensor(SensorDTO sensorDTO){
        Device device = deviceRepository.findById(sensorDTO.getDevice_id()).get();
        Sensor sensor = SensorBuilder.toEntitySensor(sensorDTO,device);
        sensorRepository.save(sensor);
        return sensor.getId();
    }

    public void deleteSensor(long id){
        Sensor sensor = sensorRepository.findById(id).get();
        List<MonitoredValueDTO> monitoredValuesDTO = monitoredValueService.findMonitoredValuesBySensorId(id);
        if(monitoredValuesDTO!=null) {
            List<MonitoredValue> monitoredValues = monitoredValuesDTO.stream()
                    .map(monitoredValueDTO -> MonitoredValueBuilder.ToEntityMonitoredValue(monitoredValueDTO, sensor))
                    .collect(Collectors.toList());
            for (MonitoredValue mv :
                    monitoredValues) {
                monitoredValueService.deleteMonitoredValue(mv.getId());
            }
        }
        sensorRepository.delete(sensor);
        LOGGER.debug("Sensor with id {} was deleted from db",id);
    }

    public void updateSensor(SensorDTO sensorDTO){
        Optional<Sensor> sensor= sensorRepository.findById(sensorDTO.getId());
        sensor.get().setSensorDescription(sensorDTO.getSensorDescription());
        sensor.get().setMaximumValue(sensorDTO.getMaximumValue());
        sensorRepository.save(sensor.get());
    }
}
