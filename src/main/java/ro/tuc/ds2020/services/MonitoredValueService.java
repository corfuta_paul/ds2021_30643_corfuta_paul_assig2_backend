package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MonitoredValueDTO;
import ro.tuc.ds2020.dtos.builders.MonitoredValueBuilder;
import ro.tuc.ds2020.entities.Client;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.MonitoredValue;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.ClientRepository;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.MonitoredValueRepository;
import ro.tuc.ds2020.repositories.SensorRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MonitoredValueService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MonitoredValueService.class);
    private final SensorRepository sensorRepository;
    private final MonitoredValueRepository monitoredValueRepository;
    private final DeviceRepository deviceRepository;
    private final ClientRepository clientRepository;
    @Autowired
    private SimpMessagingTemplate template;
    public static final String newRequest = "/topic/socket/user/";

    @Autowired
    private MonitoredValueService(SensorRepository sensorRepository, MonitoredValueRepository monitoredValueRepository, DeviceRepository deviceRepository, ClientRepository clientRepository){
        this.sensorRepository=sensorRepository;
        this.monitoredValueRepository=monitoredValueRepository;
        this.deviceRepository = deviceRepository;
        this.clientRepository = clientRepository;
    }

    public List<MonitoredValueDTO> findMonitoredValues(){
        List<MonitoredValue> monitoredValues = monitoredValueRepository.findAll();
        return monitoredValues.stream()
                .map(MonitoredValueBuilder::ToMonitoredValueDTO)
                .collect(Collectors.toList());
    }

    public MonitoredValueDTO findMonitoredValueById(long id){
        Optional<MonitoredValue> monitoredValue=monitoredValueRepository.findById(id);
        if(!monitoredValue.isPresent()){
            LOGGER.error("Monitored value with id {} was not found in db", id);
            throw new ResourceNotFoundException(MonitoredValue.class.getSimpleName() + " with id: " + id);
        }
        return MonitoredValueBuilder.ToMonitoredValueDTO(monitoredValue.get());
    }

    public List<MonitoredValueDTO> findMonitoredValuesBySensorId(long id){
        Sensor sensor = sensorRepository.findById(id).get();
        List<MonitoredValue> monitoredValues = monitoredValueRepository.findMonitoredValuesBySensor(sensor);

        if(monitoredValues!=null) {
            return monitoredValues.stream()
                    .map(MonitoredValueBuilder::ToMonitoredValueDTO)
                    .collect(Collectors.toList());
        }
        return null;
    }

    public long insertMonitoredValue(MonitoredValueDTO monitoredValueDTO){
        List<MonitoredValueDTO> monitoredValues = findMonitoredValuesBySensorId(monitoredValueDTO.getSensor_id());
        long suma=0;
        for (MonitoredValueDTO m:
             monitoredValues) {
            suma+=m.getEnergyConsumption();
        }

        if(suma+monitoredValueDTO.getEnergyConsumption()>1000) {
            Sensor sensor = sensorRepository.findById(monitoredValueDTO.getSensor_id()).get();
            Device device = deviceRepository.findById(sensor.getDevice().getId()).get();
            Client client = clientRepository.findById(device.getClient().getId()).get();
            this.template.convertAndSend(newRequest + client.getUser().getId(), "Ati depasit limita maxima!");
        }
        Sensor sensor = sensorRepository.findById(monitoredValueDTO.getSensor_id()).get();
        MonitoredValue monitoredValue = MonitoredValueBuilder.ToEntityMonitoredValue(monitoredValueDTO,sensor);
        monitoredValueRepository.save(monitoredValue);
        return monitoredValue.getId();
    }

    public void deleteMonitoredValue(long id){
        MonitoredValue monitoredValue = monitoredValueRepository.findById(id).get();
        monitoredValueRepository.delete(monitoredValue);
        LOGGER.debug("Monitored value with id {} was deleted",id);
    }

    public void updateMonitoredValue(MonitoredValueDTO monitoredValueDTO){
        Optional<MonitoredValue> monitoredValue= monitoredValueRepository.findById(monitoredValueDTO.getId());
        monitoredValue.get().setEnergyConsumption(monitoredValueDTO.getEnergyConsumption());
        monitoredValue.get().setTimeStamp(monitoredValueDTO.getTimeStamp());
        monitoredValueRepository.save(monitoredValue.get());
    }
}
