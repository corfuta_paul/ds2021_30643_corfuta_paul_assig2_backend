package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.ClientDTO;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.builders.ClientBuilder;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.entities.ApplicationUser;
import ro.tuc.ds2020.entities.Client;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.repositories.ClientRepository;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientService.class);
    private final ClientRepository clientRepository;
    private final UserRepository userRepository;
    private final DeviceService deviceService;

    @Autowired
    private ClientService(ClientRepository clientRepository, UserRepository userRepository, DeviceService deviceService){
        this.clientRepository=clientRepository;
        this.userRepository=userRepository;
        this.deviceService=deviceService;
    }

    public List<ClientDTO> findClients() {
        List<Client> clientList = clientRepository.findAll();
        return clientList.stream()
                .map(ClientBuilder::ToClientDTO)
                .collect(Collectors.toList());
    }

    public ClientDTO findClientById(long id) {
        Client client = clientRepository.findById(id).get();

        return ClientBuilder.ToClientDTO(client);
    }

    public ClientDTO findClientByUserId(long id){
        ApplicationUser user = userRepository.findById(id).get();
        Client client = clientRepository.findClientByUser(user);
        if(client!=null){
            return ClientBuilder.ToClientDTO(client);
        }
        return null;
    }

    public Long insertClient(ClientDTO clientDTO) {

        ApplicationUser user = userRepository.findById(clientDTO.getUser_id()).get();
        Client client = ClientBuilder.ToClientEntity(clientDTO,user);
        client = clientRepository.save(client);
        LOGGER.debug("Person with id {} was inserted in db", clientDTO.getUser_id());
        return client.getId();
    }

    public void deleteClient(long id) {
        Client client= clientRepository.findById(id).get();
        List<DeviceDTO> devicesDTO = deviceService.findDevicesByClientId(id);
        if(devicesDTO!=null) {
            List<Device> devices = devicesDTO.stream()
                    .map(deviceDTO -> DeviceBuilder.ToDeviceEntity(deviceDTO, client))
                    .collect(Collectors.toList());
            for (Device d :
                    devices) {
                deviceService.deleteDevice(d.getId());
            }
        }
        clientRepository.delete(client);
        LOGGER.debug("Client with id {} was deleted from db", client.getId());
    }

    public void updateClient(ClientDTO clientDTO){
        Optional<Client> client= clientRepository.findById(clientDTO.getId());
        client.get().setFirstName(clientDTO.getFirstName());
        client.get().setLastName(clientDTO.getLastName());
        client.get().setAddress(clientDTO.getAddress());
        client.get().setBirthDate(clientDTO.getBirthDate());
        clientRepository.save(client.get());
    }



}
