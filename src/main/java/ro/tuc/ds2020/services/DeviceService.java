package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.ClientDTO;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.dtos.builders.SensorBuilder;
import ro.tuc.ds2020.entities.Client;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.ClientRepository;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.SensorRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DeviceService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceService.class);
    private final DeviceRepository deviceRepository;
    private final ClientRepository clientRepository;
    private final SensorService sensorService;

    @Autowired
    private DeviceService(DeviceRepository deviceRepository, ClientRepository clientRepository,SensorService sensorService){
        this.deviceRepository=deviceRepository;
        this.clientRepository=clientRepository;
        this.sensorService=sensorService;
    }

    public List<DeviceDTO> findDevices() {
        List<Device> deviceList = deviceRepository.findAll();
        return deviceList.stream()
                .map(DeviceBuilder::ToDeviceDTO)
                .collect(Collectors.toList());
    }

    public DeviceDTO findDeviceById(long id) {
        Optional<Device> device = deviceRepository.findById(id);
        if (!device.isPresent()) {
            LOGGER.error("Device with id {} was not found in db", id);
            throw new ResourceNotFoundException(Device.class.getSimpleName() + " with id: " + id);
        }
        return DeviceBuilder.ToDeviceDTO(device.get());
    }

    public List<DeviceDTO> findDevicesByClientId(long id){
        Client client = clientRepository.findById(id).get();
        List<Device> devices = deviceRepository.findDevicesByClient(client);

        if(devices!=null)
        {
            return devices.stream()
                    .map(DeviceBuilder::ToDeviceDTO)
                    .collect(Collectors.toList());
        }
        return null;
    }

    public long insertDevice(DeviceDTO deviceDTO){
        Client client = clientRepository.findById(deviceDTO.getClient_id()).get();
        Device device = DeviceBuilder.ToDeviceEntity(deviceDTO,client);
        deviceRepository.save(device);
        return device.getId();
    }

    public void deleteDevice(long id){
        Device device = deviceRepository.findById(id).get();
        SensorDTO sensorDTO = sensorService.findSensorByDeviceId(id);
        if(sensorDTO!=null)
        {
            Sensor sensor = SensorBuilder.toEntitySensor(sensorDTO,device);
            sensorService.deleteSensor(sensor.getId());
        }
        deviceRepository.delete(device);
        LOGGER.debug("Device with id {} was deleted from db", device.getId());
    }

    public void updateDevice(DeviceDTO deviceDTO){
        Optional<Device> device= deviceRepository.findById(deviceDTO.getId());
        device.get().setAddress(deviceDTO.getAddress());
        device.get().setDescription(deviceDTO.getDescription());
        device.get().setMaximumEnergyConsumption(deviceDTO.getMaximumEnergyConsumption());
        device.get().setAverageEnergyConsumption(deviceDTO.getAverageEnergyConsumption());
        deviceRepository.save(device.get());
    }

    public long findMaximumConsumptionbyDevice(long id){
        return deviceRepository.findMaximumConsumptionByDevice(id);
    }
}
