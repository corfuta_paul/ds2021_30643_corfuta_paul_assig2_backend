package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.ClientDTO;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.builders.ClientBuilder;
import ro.tuc.ds2020.dtos.builders.SensorBuilder;
import ro.tuc.ds2020.dtos.builders.UserBuilder;
import ro.tuc.ds2020.entities.ApplicationUser;
import ro.tuc.ds2020.entities.Client;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.ClientRepository;
import ro.tuc.ds2020.repositories.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final UserRepository userRepository;
    private final ClientService clientService;

    @Autowired
    private UserService(UserRepository userRepository,ClientService clientService){
        this.userRepository=userRepository;
        this.clientService=clientService;
    }

    public List<UserDTO> findUsers() {
        List<ApplicationUser> userList = userRepository.findAll();
        return userList.stream()
                .map(UserBuilder::toUserDTO)
                .collect(Collectors.toList());
    }

    public Long insertUser(UserDTO userDTO) {
        ApplicationUser user = UserBuilder.ToEntityUser(userDTO);
        user = userRepository.save(user);
        LOGGER.debug("User with id {} was inserted in db", user.getId());
        return user.getId();
    }

    public void deleteUser(long id) {
        ApplicationUser user = userRepository.findById(id).get();
        ClientDTO clientDTO = clientService.findClientByUserId(id);
        if(clientDTO!=null)
        {
            Client client = ClientBuilder.ToClientEntity(clientDTO,user);
            clientService.deleteClient(client.getId());
        }
        userRepository.delete(user);
        LOGGER.debug("User with id {} was deleted from db", user.getId());
    }

    public void updateUser(UserDTO userDTO){
        Optional<ApplicationUser> user= userRepository.findById(userDTO.getId());
        user.get().setUsername(userDTO.getUsername());
        user.get().setPassword(userDTO.getPassword());
        user.get().setAdmin(userDTO.isAdmin());
        userRepository.save(user.get());
    }

    public UserDTO findUserById(long id){
        Optional<ApplicationUser> user = userRepository.findById(id);
        if(!user.isPresent()){
            LOGGER.error("User with id {} was not found in db", id);
            throw new ResourceNotFoundException(ApplicationUser.class.getSimpleName() + " with id: " + id);
        }
        return UserBuilder.toUserDTO(user.get());
    }

    public UserDTO login(UserDTO userDTO){
         List<ApplicationUser> users= userRepository.findAll();
        for (ApplicationUser user:
             users) {
            if(user.getUsername().equals(userDTO.getUsername()) && user.getPassword().equals(userDTO.getPassword())) {
                return UserBuilder.toUserDTO(user);
            }
        }
        return null;
    }

}
