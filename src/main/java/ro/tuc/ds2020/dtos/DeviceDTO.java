package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class DeviceDTO extends RepresentationModel<DeviceDTO> {
    private long id;
    private String description;
    private String address;
    private long maximumEnergyConsumption;
    private long averageEnergyConsumption;
    private long client_id;
}
