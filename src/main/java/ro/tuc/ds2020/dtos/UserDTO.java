package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class UserDTO extends RepresentationModel<UserDTO> {
    private long id;
    private String username;
    private String password;
    private boolean admin;
}
