package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.entities.Client;
import ro.tuc.ds2020.entities.Device;

public class DeviceBuilder {

    private DeviceBuilder(){

    }

    public static DeviceDTO ToDeviceDTO(Device device){
        return new DeviceDTO(
                device.getId(),
                device.getDescription(),
                device.getAddress(),
                device.getMaximumEnergyConsumption(),
                device.getAverageEnergyConsumption(),
                device.getClient().getId()
        );
    }

    public static Device ToDeviceEntity(DeviceDTO deviceDTO, Client client){
        return new Device(
                deviceDTO.getId(),
                deviceDTO.getDescription(),
                deviceDTO.getAddress(),
                deviceDTO.getMaximumEnergyConsumption(),
                deviceDTO.getAverageEnergyConsumption(),
                client
        );
    }
}
