package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.ClientDTO;
import ro.tuc.ds2020.entities.ApplicationUser;
import ro.tuc.ds2020.entities.Client;

public class ClientBuilder {

    private ClientBuilder(){

    }

    public static ClientDTO ToClientDTO(Client client){
        return new ClientDTO(client.getId(),
                client.getFirstName(),
                client.getLastName(),
                client.getBirthDate(),
                client.getAddress(),
                client.getUser().getId());
    }

    public static Client ToClientEntity(ClientDTO clientDTO, ApplicationUser user) {

        return new Client(
                clientDTO.getId(),
                clientDTO.getFirstName(),
                clientDTO.getLastName(),
                clientDTO.getBirthDate(),
                clientDTO.getAddress(),
                user
        );
    }
}
