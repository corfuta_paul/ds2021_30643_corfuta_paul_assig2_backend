package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Sensor;

public class SensorBuilder {
    private SensorBuilder(){

    }

    public static SensorDTO ToSensorDTO(Sensor sensor){
        return new SensorDTO(
                sensor.getId(),
                sensor.getSensorDescription(),
                sensor.getMaximumValue(),
                sensor.getDevice().getId()
        );
    }

    public static Sensor toEntitySensor(SensorDTO sensorDTO, Device device){
        return new Sensor(
                sensorDTO.getId(),
                sensorDTO.getSensorDescription(),
                sensorDTO.getMaximumValue(),
                device
        );
    }
}
