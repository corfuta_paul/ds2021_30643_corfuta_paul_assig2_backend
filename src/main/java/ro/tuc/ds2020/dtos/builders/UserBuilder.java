package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.entities.ApplicationUser;

public class UserBuilder {

    private UserBuilder(){
    }

    public static ApplicationUser ToEntityUser (UserDTO userDTO){
        return new ApplicationUser(
                userDTO.getId(),
                userDTO.getUsername(),
                userDTO.getPassword(),
                userDTO.isAdmin()
        );
    }

    public static UserDTO toUserDTO (ApplicationUser user){
        return new UserDTO(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.isAdmin()
        );
    }
}
