package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;


@Data
@AllArgsConstructor
@NoArgsConstructor

public class SensorDTO extends RepresentationModel<SensorDTO> {

    private long id;
    private String sensorDescription;
    private long maximumValue;
    private long device_id;
}
